#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <glimac/FilePath.hpp>
#include <glimac/Program.hpp>
#include <glimac/glm.hpp>
#include <glimac/common.hpp>
#include <iostream>
#include <vector>
#include <glimac/Cone.hpp>

using namespace glm;

int window_width  = 1280;
int window_height = 720;
float moovePerTick  = 5 / 10000;
bool inMoovement = 0;
bool  escapePressed = false;
float alpha = 0;
float betha = 0;

float l = 15;
float d = 5;
float E = 1;

static void key_callback(GLFWwindow* /*window*/, int key, int /*scancode*/, int action, int /*mods*/)
{
    if (key == GLFW_KEY_E && action == GLFW_PRESS) {
        inMoovement = !inMoovement;
    }

    if (key == GLFW_KEY_R && action == GLFW_PRESS) {
        moovePerTick += 1/10000;
    }

    if (key == GLFW_KEY_F && action == GLFW_PRESS) {
        moovePerTick -= 1 / 10000;
    }

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        escapePressed = true;
    }
}

static void mouse_button_callback(GLFWwindow* /*window*/, int /*button*/, int /*action*/, int /*mods*/)
{
}

static void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double /*yoffset*/)
{
}

static void cursor_position_callback(GLFWwindow* /*window*/, double /*xpos*/, double /*ypos*/)
{
}

static void size_callback(GLFWwindow* /*window*/, int width, int height)
{
    window_width  = width;
    window_height = height;
}

int main(int argc, char** argv)
{
    /* Initialize the library */
    if (!glfwInit()) {
        return -1;
    }

    /* Create a window and its OpenGL context */
#ifdef __APPLE__
    /* We need to explicitly ask for a 3.3 context on Mac */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
    GLFWwindow* window = glfwCreateWindow(window_width, window_height, "Proj", nullptr, nullptr);
    if (!window) {
        glfwTerminate();
        return -1;
    }
    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    /* Intialize glad (loads the OpenGL functions) */
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        return -1;
    }

    glimac::Cone cone(1, 1, 32, 16);

     GLuint vbo;                         // Vertex buffer object -> liste de nos buffer de point
    glGenBuffers(1, &vbo);              // Attribution de l'espace memoire, ici 1 vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo); // Binding du vbo pour pouvoir le modifier

    glBufferData(GL_ARRAY_BUFFER, sizeof(glimac::ShapeVertex) * cone.getVertexCount(), cone.getDataPointer(), GL_STATIC_DRAW); // remplissage de notre vbo
    glBindBuffer(GL_ARRAY_BUFFER, 0);                                                              // D�binding du vbo pour ne pas le modifier par erreur

    GLuint ibo; // Index buffer object
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo); //cible r�serv� pour les ibo
    std::vector<uint32_t> indices;              // tableau d'indice
    for (int i = 0; i < cone.getVertexCount() - 1; i++) {
        indices.push_back(i);
    }
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, cone.getVertexCount() * sizeof(uint32_t), indices.data(), GL_STATIC_DRAW); //On rempli le ibo
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);                                                        //on debind

    GLuint vao;                                 // Vertex array object -> pour interpr�ter le vbo
    glGenVertexArrays(1, &vao);                 // Cr�ation du VAO
    glBindVertexArray(vao);                     // Binding du vao, pas de cible car une seul cible possible
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo); //bind du ibo pour l'enregistrer dans le vba
    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_NORMAL = 1;
    const GLuint VERTEX_ATTR_TEXTURE  = 2;
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 0, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, position));
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 1, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, normal));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, texCoords));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glimac::FilePath applicationPath(argv[0]);
    glimac::Program  program = glimac::loadProgram(applicationPath.dirPath() + ("Proj/shaders/3D.vs.glsl"),
                                           applicationPath.dirPath() + ("Proj/shaders/normals.fs.glsl"));
    program.use();
    
    glGetUniformLocation;
    glEnable(GL_DEPTH_TEST);

    mat4 ProjMatrix;
    
    mat4 BrasMVMatrixIni;
    mat4 BrasMVMatrix;
    mat4 BrasNormalMatrix;

    mat4 ArbreMVMatrixIni;
    mat4 ArbreMVMatrix;
    mat4 ArbreNormalMatrix;

    mat4 RailMVMatrixIni;
    mat4 RailNormalMatrix;

    ProjMatrix = perspective(radians(70.f), 1.5f, 0.1f, 100.f);
    //Positions initial
    //On veut tous les composants sur la meme ligne
    BrasMVMatrixIni  = scale(BrasMVMatrixIni, vec3(E, 2 * d + E, E));          //Taille du bras
    BrasMVMatrixIni  = translate(BrasMVMatrixIni, vec3(-(l + d) / 2, 0, -10.0f)); //On le place � l'extremit�du rail
    BrasMVMatrixIni  = rotate(BrasMVMatrixIni, 3.1415f, vec3(0, 0, 1));           //on l'oriente sur laxe horizontal
    BrasNormalMatrix = transpose(inverse(BrasMVMatrixIni));

    ArbreMVMatrixIni  = scale(ArbreMVMatrixIni, vec3(E, l, E));              //Taille de l'arbre
    ArbreMVMatrixIni  = translate(ArbreMVMatrixIni, vec3(d / 2, 0, -10.0f)); //On place l'abre
    ArbreMVMatrixIni  = rotate(ArbreMVMatrixIni, -3.1415f, vec3(0, 0, 1)); //on l'oriente sur laxe horizontal
    ArbreNormalMatrix = transpose(inverse(ArbreMVMatrixIni));

    RailMVMatrixIni = scale(RailMVMatrixIni, vec3(2 * E * E, l + d + 2 * E, E / 4)); //Taille du rail
    RailMVMatrixIni = rotate(RailMVMatrixIni, -3.1415f, vec3(0, 0, 1));           //on l'oriente sur laxe horizontal
    RailMVMatrixIni = translate(RailMVMatrixIni, vec3(0, 0, -10.0f));                //Le rail est au centre de la sc�ne
    RailNormalMatrix = transpose(inverse(RailMVMatrixIni));
    /* Hook input callbacks */
    glfwSetKeyCallback(window, &key_callback);
    glfwSetMouseButtonCallback(window, &mouse_button_callback);
    glfwSetScrollCallback(window, &scroll_callback);
    glfwSetCursorPosCallback(window, &cursor_position_callback);
    glfwSetWindowSizeCallback(window, &size_callback);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        if (escapePressed) {
            glfwTerminate();
            return 0;
        }
        
        alpha += inMoovement*moovePerTick;
        betha = asin(-sin(alpha) * d / l);

        glClearColor(1.f, 0.5f, 0.5f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //mouvement du bras
        BrasMVMatrix = rotate(BrasMVMatrixIni, alpha, vec3(0, 0, 1));
        //affichage du bras
        glUniformMatrix4fv(0, 1, false, value_ptr(ProjMatrix * BrasMVMatrix));
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, cone.getVertexCount());
        glBindVertexArray(0);

        //mouvement de l'arbre
        ArbreMVMatrix = translate(ArbreMVMatrixIni, vec3(0, cos(alpha)*2*d, 0));
        ArbreMVMatrix = rotate(ArbreMVMatrix, betha - alpha, vec3(0, 0, 1));
        //affichage de l'arbre
        glUniformMatrix4fv(0, 1, false, value_ptr(ProjMatrix * ArbreMVMatrix));
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, cone.getVertexCount());
        glBindVertexArray(0);

        //affichage du rail
        glUniformMatrix4fv(0, 1, false, value_ptr(ProjMatrix * RailMVMatrixIni));
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, cone.getVertexCount());
        glBindVertexArray(0);
        /* Swap front and back buffers */
        glfwSwapBuffers(window);
        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}